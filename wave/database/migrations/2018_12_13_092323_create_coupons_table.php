<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ProductCategory_id');
            $table->integer('Store_id');
            $table->string('type', 255);
            $table->string('title', 255);
            $table->string('nano_title', 255);
            $table->string('url', 255);
            $table->text('description');
            $table->string('coupon_code', 255);
            $table->string('coupon_discount', 255);
            $table->string('tnc_customer_type', 255);
            $table->string('tnc_brand', 255);
            $table->integer('tnc_min_order');
            $table->text('tnc_description');
            $table->integer('list_order');
            $table->tinyInteger('pin');
            $table->tinyInteger('status');
            $table->timestamps('date_start');
            $table->timestamps('date_end');
            $table->timestamps('date_create');
            $table->timestamps('date_modify');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
