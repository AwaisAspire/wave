@extends('voyager::master')
@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)
@section('css')
<style>
    .panel .mce-panel {
        border-left-color: #fff;
        border-right-color: #fff;
    }

    .panel .mce-toolbar,
    .panel .mce-statusbar {
        padding-left: 20px;
    }

    .panel .mce-edit-area,
    .panel .mce-edit-area iframe,
    .panel .mce-edit-area iframe html {
        padding: 0 10px;
        min-height: 350px;
    }

    .mce-content-body {
        color: #555;
        font-size: 14px;
    }

    .panel.is-fullscreen .mce-statusbar {
        position: absolute;
        bottom: 0;
        width: 100%;
        z-index: 200000;
    }

    .panel.is-fullscreen .mce-tinymce {
        height:100%;
    }

    .panel.is-fullscreen .mce-edit-area,
    .panel.is-fullscreen .mce-edit-area iframe,
    .panel.is-fullscreen .mce-edit-area iframe html {
        height: 100%;
        position: absolute;
        width: 99%;
        overflow-y: scroll;
        overflow-x: hidden;
        min-height: 100%;
    }
</style>
@stop
@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form" action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif" method="POST" enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        @if(isset($dataTypeContent->id))
        {{ method_field("PUT") }}
        @endif
        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-7">
                <!-- ### TITLE ### -->
                <div class="panel">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <?php 
                        $panel_title='Details';
                        $include = ['type','ProductCategory_id','title','coupon_hasone_store_relationship','nano_title','url','description'];
                        echo view('customfields', compact('dataType', 'dataTypeContent', 'isModelTranslatable','stores','include','panel_title'));
                        ?>
                </div>
                <div class="panel">
                        <?php 
                        $panel_title='Coupon';
                        $include = ['coupon_code','coupon_discount','pin','status'];
                        echo view('customfields', compact('dataType', 'dataTypeContent', 'isModelTranslatable','stores','include','panel_title'));
                        ?>
                </div>

            </div>
            <div class="col-md-5">
                <div class="panel panel panel-bordered panel-warning">
                        <?php 
                        $panel_title='Terms and Conditions';
                        $include = ['tnc_customer_type','tnc_brand','tnc_min_order','list_order','tnc_description'];
                        echo view('customfields', compact('dataType', 'dataTypeContent', 'isModelTranslatable','stores','include','panel_title'));
                        ?>
                </div>
                <div class="panel panel panel-bordered panel-info">
                        <?php 
                        $panel_title='Other';
                        $include = ['date_start','date_end'];
                        $lifetime=1;
                        echo view('customfields', compact('dataType', 'dataTypeContent', 'isModelTranslatable','stores','include','lifetime','panel_title'));
                        ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
        </div>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
               onchange="$('#my_form').submit(); this.value = '';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        {{ csrf_field() }}
    </form>

</div>
<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
            </div>

            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    var params = {};
    var $image;
    $('document').ready(function () {
    $('.toggleswitch').bootstrapToggle();
    //Init datepicker for date fields if data-datepicker attribute defined
    //or if browser does not handle date inputs
    $('.form-group input[type=date]').each(function (idx, elt) {
    if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
    elt.type = 'text';
    $(elt).datetimepicker($(elt).data('datepicker'));
    }
    });
    @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
    @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
    $(el).slugify();
    });
    $('.form-group').on('click', '.remove-multi-image', function (e) {
    e.preventDefault();
    $image = $(this).siblings('img');
    params = {
    slug:   '{{ $dataType->slug }}',
            image:  $image.data('image'),
            id:     $image.data('id'),
            field:  $image.parent().data('field-name'),
            _token: '{{ csrf_token() }}'
    }

    $('.confirm_delete_name').text($image.data('image'));
    $('#confirm_delete_modal').modal('show');
    });
    $('#confirm_delete').on('click', function(){
    $.post('{{ route('voyager.media.remove') }}', params, function (response) {
    if (response
            && response.data
            && response.data.status
            && response.data.status == 200) {

    toastr.success(response.data.message);
    $image.parent().fadeOut(300, function() { $(this).remove(); })
    } else {
    toastr.error("Error removing image.");
    }
    });
    $('#confirm_delete_modal').modal('hide');
    });
    $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop
