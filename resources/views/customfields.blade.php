<div class="panel-heading">
    <h3 class="panel-title"><?php echo $panel_title;?></h3>
    <div class="panel-actions">
        <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
    </div>
</div>
<div class="panel-body">
    @php
    $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
    @endphp
    @foreach($dataTypeRows as $row)
    @if(in_array($row->field, $include))
    @php
    $display_options = isset($row->details->display) ? $row->details->display : NULL;
    @endphp
    @if (isset($row->details->formfields_custom))
    @include('voyager::formfields.custom.' . $row->details->formfields_custom)
    @else
    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ isset($display_options->width) ? $display_options->width : 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
        {{ $row->slugify }}
        <label for="name">{{ $row->display_name }}</label>
        @include('voyager::multilingual.input-hidden-bread-edit-add')
        @if($row->type == 'relationship')
            @include('voyager::formfields.relationship', ['options' => $row->details])      
        @else
            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
        @endif

        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
        @endforeach
    </div>
    @endif
    @endif
    @endforeach
    <?php if(isset($lifetime)) { ?>
    <div class="form-group">
        <label for="name">Life Time</label>
        <input type="checkbox" name="life_time" value="Yes"/>
    </div>
    <?php } ?>
</div>