<?php

class formfields_helper
{
    public static function render_fields($data_string)
    {
        $data = unserialize($data_string);
        $dataTypeRows = json_decode(json_encode($data['dataTypeRows']), true);
        $dataTypeContent = json_decode(json_encode($data['dataTypeContent']), true);
        $dataType = json_decode(json_encode($data['dataType']), true);
        $include = json_decode(json_encode($data['include']), true);

//        $dataTypeRows=$data['dataTypeRows'];
//        $dataTypeContent=$data['dataTypeContent'];
//        $dataType=$data['dataType'];
//        echo "<pre>";
//       foreach($dataTypeRows as $row)
//        print_r($row['type'].'<br>');
//        exit;

        $field = '
            @foreach($dataTypeRows as $row)
            @if(in_array($row->field, $include))
            @php
            $display_options = isset($row->details->display) ? $row->details->display : NULL;
            @endphp
            @if (isset($row->details->formfields_custom))
            @include("voyager::formfields.custom." . $row->details->formfields_custom)
            @else
            <div class="form-group @if($row->type == "hidden") hidden @endif @if(isset($display_options->width)){{ "col-md-" . $display_options->width }}@endif" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                 {{ $row->slugify }}
                 <label for="name">{{ $row->display_name }}</label>
                @include("voyager::multilingual.input-hidden-bread-edit-add")
                @if($row->type == "relationship")
                @include("voyager::formfields.relationship")
                @else
                {!! app("voyager")->formField($row, $dataType, $dataTypeContent) !!}
                @endif

                @foreach (app("voyager")->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                @endforeach
            </div>
            @endif
            @endif
            @endforeach
            
        </div>
            ';
        echo '<pre>';
        print_r($field);
        exit;

        return $field;
    }
}
?>

