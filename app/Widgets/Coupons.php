<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class Coupons extends \TCG\Voyager\Widgets\BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\Coupon::count();
        $string = 'Coupons';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-check-circle',
            'title'  => "{$count} {$string}",
            'text'   => 'Coupons',
            'button' => [
                'text' => 'View all Coupons',
                'link' => route('voyager.coupons.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/coupon.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('Post'));
    }
}
