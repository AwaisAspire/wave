<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('permission_role')->delete();

        \DB::table('permission_role')->insert([
            0 => [
                'permission_id' => 1,
                'role_id'       => 1,
            ],
            1 => [
                'permission_id' => 2,
                'role_id'       => 1,
            ],
            2 => [
                'permission_id' => 3,
                'role_id'       => 1,
            ],
            3 => [
                'permission_id' => 4,
                'role_id'       => 1,
            ],
            4 => [
                'permission_id' => 5,
                'role_id'       => 1,
            ],
            5 => [
                'permission_id' => 6,
                'role_id'       => 1,
            ],
            6 => [
                'permission_id' => 6,
                'role_id'       => 2,
            ],
            7 => [
                'permission_id' => 6,
                'role_id'       => 3,
            ],
            8 => [
                'permission_id' => 6,
                'role_id'       => 4,
            ],
            9 => [
                'permission_id' => 6,
                'role_id'       => 5,
            ],
            10 => [
                'permission_id' => 7,
                'role_id'       => 1,
            ],
            11 => [
                'permission_id' => 7,
                'role_id'       => 2,
            ],
            12 => [
                'permission_id' => 7,
                'role_id'       => 3,
            ],
            13 => [
                'permission_id' => 7,
                'role_id'       => 4,
            ],
            14 => [
                'permission_id' => 7,
                'role_id'       => 5,
            ],
            15 => [
                'permission_id' => 8,
                'role_id'       => 1,
            ],
            16 => [
                'permission_id' => 9,
                'role_id'       => 1,
            ],
            17 => [
                'permission_id' => 10,
                'role_id'       => 1,
            ],
            18 => [
                'permission_id' => 11,
                'role_id'       => 1,
            ],
            19 => [
                'permission_id' => 12,
                'role_id'       => 1,
            ],
            20 => [
                'permission_id' => 13,
                'role_id'       => 1,
            ],
            21 => [
                'permission_id' => 14,
                'role_id'       => 1,
            ],
            22 => [
                'permission_id' => 15,
                'role_id'       => 1,
            ],
            23 => [
                'permission_id' => 16,
                'role_id'       => 1,
            ],
            24 => [
                'permission_id' => 16,
                'role_id'       => 3,
            ],
            25 => [
                'permission_id' => 16,
                'role_id'       => 4,
            ],
            26 => [
                'permission_id' => 16,
                'role_id'       => 5,
            ],
            27 => [
                'permission_id' => 17,
                'role_id'       => 1,
            ],
            28 => [
                'permission_id' => 17,
                'role_id'       => 3,
            ],
            29 => [
                'permission_id' => 17,
                'role_id'       => 4,
            ],
            30 => [
                'permission_id' => 17,
                'role_id'       => 5,
            ],
            31 => [
                'permission_id' => 18,
                'role_id'       => 1,
            ],
            32 => [
                'permission_id' => 19,
                'role_id'       => 1,
            ],
            33 => [
                'permission_id' => 20,
                'role_id'       => 1,
            ],
            34 => [
                'permission_id' => 21,
                'role_id'       => 1,
            ],
            35 => [
                'permission_id' => 22,
                'role_id'       => 1,
            ],
            36 => [
                'permission_id' => 23,
                'role_id'       => 1,
            ],
            37 => [
                'permission_id' => 24,
                'role_id'       => 1,
            ],
            38 => [
                'permission_id' => 25,
                'role_id'       => 1,
            ],
            39 => [
                'permission_id' => 26,
                'role_id'       => 1,
            ],
            40 => [
                'permission_id' => 26,
                'role_id'       => 2,
            ],
            41 => [
                'permission_id' => 26,
                'role_id'       => 3,
            ],
            42 => [
                'permission_id' => 26,
                'role_id'       => 4,
            ],
            43 => [
                'permission_id' => 26,
                'role_id'       => 5,
            ],
            44 => [
                'permission_id' => 27,
                'role_id'       => 1,
            ],
            45 => [
                'permission_id' => 27,
                'role_id'       => 2,
            ],
            46 => [
                'permission_id' => 27,
                'role_id'       => 3,
            ],
            47 => [
                'permission_id' => 27,
                'role_id'       => 4,
            ],
            48 => [
                'permission_id' => 27,
                'role_id'       => 5,
            ],
            49 => [
                'permission_id' => 28,
                'role_id'       => 1,
            ],
            50 => [
                'permission_id' => 29,
                'role_id'       => 1,
            ],
            51 => [
                'permission_id' => 30,
                'role_id'       => 1,
            ],
            52 => [
                'permission_id' => 31,
                'role_id'       => 1,
            ],
            53 => [
                'permission_id' => 31,
                'role_id'       => 2,
            ],
            54 => [
                'permission_id' => 31,
                'role_id'       => 3,
            ],
            55 => [
                'permission_id' => 31,
                'role_id'       => 4,
            ],
            56 => [
                'permission_id' => 31,
                'role_id'       => 5,
            ],
            57 => [
                'permission_id' => 32,
                'role_id'       => 1,
            ],
            58 => [
                'permission_id' => 32,
                'role_id'       => 2,
            ],
            59 => [
                'permission_id' => 32,
                'role_id'       => 3,
            ],
            60 => [
                'permission_id' => 32,
                'role_id'       => 4,
            ],
            61 => [
                'permission_id' => 32,
                'role_id'       => 5,
            ],
            62 => [
                'permission_id' => 33,
                'role_id'       => 1,
            ],
            63 => [
                'permission_id' => 34,
                'role_id'       => 1,
            ],
            64 => [
                'permission_id' => 35,
                'role_id'       => 1,
            ],
            65 => [
                'permission_id' => 36,
                'role_id'       => 1,
            ],
            66 => [
                'permission_id' => 36,
                'role_id'       => 2,
            ],
            67 => [
                'permission_id' => 36,
                'role_id'       => 3,
            ],
            68 => [
                'permission_id' => 36,
                'role_id'       => 4,
            ],
            69 => [
                'permission_id' => 36,
                'role_id'       => 5,
            ],
            70 => [
                'permission_id' => 37,
                'role_id'       => 1,
            ],
            71 => [
                'permission_id' => 37,
                'role_id'       => 2,
            ],
            72 => [
                'permission_id' => 37,
                'role_id'       => 3,
            ],
            73 => [
                'permission_id' => 37,
                'role_id'       => 4,
            ],
            74 => [
                'permission_id' => 37,
                'role_id'       => 5,
            ],
            75 => [
                'permission_id' => 38,
                'role_id'       => 1,
            ],
            76 => [
                'permission_id' => 39,
                'role_id'       => 1,
            ],
            77 => [
                'permission_id' => 40,
                'role_id'       => 1,
            ],
            78 => [
                'permission_id' => 41,
                'role_id'       => 1,
            ],
            79 => [
                'permission_id' => 42,
                'role_id'       => 1,
            ],
            80 => [
                'permission_id' => 42,
                'role_id'       => 2,
            ],
            81 => [
                'permission_id' => 42,
                'role_id'       => 3,
            ],
            82 => [
                'permission_id' => 42,
                'role_id'       => 4,
            ],
            83 => [
                'permission_id' => 42,
                'role_id'       => 5,
            ],
            84 => [
                'permission_id' => 43,
                'role_id'       => 1,
            ],
            85 => [
                'permission_id' => 43,
                'role_id'       => 2,
            ],
            86 => [
                'permission_id' => 43,
                'role_id'       => 3,
            ],
            87 => [
                'permission_id' => 43,
                'role_id'       => 4,
            ],
            88 => [
                'permission_id' => 43,
                'role_id'       => 5,
            ],
            89 => [
                'permission_id' => 44,
                'role_id'       => 1,
            ],
            90 => [
                'permission_id' => 45,
                'role_id'       => 1,
            ],
            91 => [
                'permission_id' => 46,
                'role_id'       => 1,
            ],
            92 => [
                'permission_id' => 47,
                'role_id'       => 1,
            ],
            93 => [
                'permission_id' => 48,
                'role_id'       => 1,
            ],
            94 => [
                'permission_id' => 49,
                'role_id'       => 1,
            ],
            95 => [
                'permission_id' => 50,
                'role_id'       => 1,
            ],
            96 => [
                'permission_id' => 51,
                'role_id'       => 1,
            ],
            97 => [
                'permission_id' => 52,
                'role_id'       => 1,
            ],
            98 => [
                'permission_id' => 53,
                'role_id'       => 1,
            ],
            99 => [
                'permission_id' => 54,
                'role_id'       => 1,
            ],
            100 => [
                'permission_id' => 55,
                'role_id'       => 1,
            ],
            101 => [
                'permission_id' => 56,
                'role_id'       => 1,
            ],
            102 => [
                'permission_id' => 57,
                'role_id'       => 1,
            ],
        ]);
    }
}
